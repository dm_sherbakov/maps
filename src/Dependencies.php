<?php

namespace App;

use App\Database\Repository\BuildingRepository;
use App\Database\Repository\CategoryRepository;
use App\Database\Repository\CompanyRepository;
use App\Services\BuildingManager;
use App\Services\CategoryManager;
use App\Services\CompanyManager;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Log\LoggerInterface;

class Dependencies
{
    public static function init(\Slim\App $app = null)
    {
        if($app == null) {
            $app = App::getInstance()->getSlim();
        }
        $container = $app->getContainer();

        $container[LoggerInterface::class] = function ($c) {
            $settings = $c->get('settings')['logger'];
            $logger = new Logger($settings['name']);
            $logger->pushProcessor(new UidProcessor());
            $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));
            return $logger;
        };

        self::initDatabaseLayer($app);
        self::initServiceLayer($app);
    }

    private static function initDatabaseLayer(\Slim\App $app){
        $container = $app->getContainer();

        $container[EntityManager::class] = function ($c)
        {
            $config = Setup::createAnnotationMetadataConfiguration(
                $c['settings']['doctrine']['meta']['entity_path'],
                $c['settings']['doctrine']['dev_mode']
            );

            $config->setMetadataDriverImpl(
                new AnnotationDriver(
                    new AnnotationReader(),
                    $c['settings']['doctrine']['meta']['entity_path']
                )
            );

            $config->setMetadataCacheImpl(
                new FilesystemCache(
                    $c['settings']['doctrine']['meta']['cache']
                )
            );

            return EntityManager::create(
                $c['settings']['doctrine']['connection'],
                $config
            );
        };

        $container[BuildingRepository::class] = function($c){
            return new BuildingRepository($c[EntityManager::class]);
        };
        $container[CategoryRepository::class] = function($c){
            return new CategoryRepository($c[EntityManager::class]);
        };
        $container[CompanyRepository::class] = function($c){
            return new CompanyRepository($c[EntityManager::class]);
        };
    }

    private static function initServiceLayer(\Slim\App $app)
    {
        $container = $app->getContainer();

        $container['BuildingManager'] = function($c){
            return new BuildingManager($c);
        };

        $container['CategoryManager'] = function($c){
            return new CategoryManager($c);
        };

        $container['CompanyManager'] = function ($c){
            return new CompanyManager($c);
        };
    }
}