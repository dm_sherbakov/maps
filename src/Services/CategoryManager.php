<?php
namespace App\Services;

use App\Database\Repository\CategoryRepository;
use App\Entity\Category;
use App\Filters\CategoryFilter;
use Slim\Container;

class CategoryManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function create(Category $category)
    {
        return $this->getCategoryRepository()->create($category);
    }

    public function findByFilter(CategoryFilter $filter)
    {

    }

    public function update(Category $category)
    {
        return $this->getCategoryRepository()->update($category);
    }

    private function getCategoryRepository()
    {
        return $this->container->get(CategoryRepository::class);
    }
}