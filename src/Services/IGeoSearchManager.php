<?php
namespace App\Services;

interface IGeoSearchManager
{
    function findByCoordinates($latitude, $longitude, $radius);
}