<?php
namespace App\Services;

use App\Database\Repository\CompanyRepository;
use App\Entity\Company;
use Slim\Container;

class CompanyManager implements IGeoSearchManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    function findByCoordinates($latitude, $longitude, $radius)
    {
        // TODO: Implement findByCoordinates() method.
    }

    public function create(Company $company)
    {
        return $this->getCompanyRepository()->create($company);
    }

    public function update(Company $company)
    {
        return $this->getCompanyRepository()->update($company);
    }

    public function findByFilter()
    {

    }

    private function getCompanyRepository()
    {
        return $this->container->get(CompanyRepository::class);
    }
}