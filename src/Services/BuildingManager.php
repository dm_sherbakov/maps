<?php
namespace App\Services;

use App\Database\Repository\BuildingRepository;
use App\Database\Repository\CategoryRepository;
use App\Database\Repository\CompanyRepository;
use App\Entity\Building;
use App\Filters\BuildingFilter;
use Slim\Container;

class BuildingManager implements IGeoSearchManager
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    function findByCoordinates($latitude, $longitude, $radius)
    {
        // TODO: Implement findByCoordinates() method.
    }

    public function findByFilter(BuildingFilter $filter)
    {

        if($filter->companyId){
            $this->container->get(CompanyRepository::class);
        }
        $list = $this->getBuildingRepository()->findByParams($filter);
    }

    public function create(Building $building)
    {
        return $this->getBuildingRepository()->create($building);
    }

    public function update(Building $building)
    {
        return $this->getBuildingRepository()->update($building);
    }

    /**
     * @return BuildingRepository
     */
    private function getBuildingRepository()
    {
        return $this->container->get(BuildingRepository::class);
    }
}