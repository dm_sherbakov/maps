<?php
namespace App\Filters;

use App\Entity\Square;

class BuildingFilter
{
    public $address;
    public $buildingId;
    public $companyId;
    private $square;

    public function setSquare(Square $square){
        $this->square = $square;
    }

    public function getSquare(){
        return $this->square;
    }
}