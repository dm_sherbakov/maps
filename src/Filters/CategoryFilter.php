<?php
namespace App\Filters;

class CategoryFilter
{
    public $categoryId;
    public $categoryName;
    public $parentCategoryId;
}