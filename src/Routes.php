<?php
namespace App;
use Slim\Http\Request;
use Slim\Http\Response;
class Routes
{
    public $app;
    public function __construct(\Slim\App $app)
    {
        $this->app = $app;
    }

    public function init(){

        $this->app->get('/building/create', function(Request $request, Response $response, array $args) {

        });
        $this->app->group('/category', function(){
            $this->post('/create', function($request, $response, $args){

            });
            $this->post('/update', function($request, $response, $args){

            });
            $this->get('/childs/{id:[0-9]+}', function($request, $response, $args){

            });
            $this->get('/childs/', function($request, $response, $args){

            });

        });

        $this->app->group('/building', function(){
            $this->get('/list', function($request, $response, $args){

            });
        });
    }
}