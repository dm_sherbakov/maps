<?php

namespace App\Entity;

interface IEntityList
{
    static function load(array $data);

    /**
     * @return IEntity[]
     */
    function getList();

    function add(IEntity $item);
}