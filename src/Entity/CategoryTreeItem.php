<?php
namespace App\Entity;

class CategoryTreeItem extends Category implements IEntityList
{
    private $categories = [];
    static function load(array $data)
    {
        // TODO: Implement load() method.
    }

    function getList()
    {
        return $this->categories;
    }

    function add(IEntity $item)
    {
        array_push($this->categories, $item);
    }

}