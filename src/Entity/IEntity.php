<?php
namespace App\Entity;

interface IEntity
{
    static function load(array $data);
}