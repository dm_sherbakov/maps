<?php
namespace App\Entity;

class Building implements IEntity
{
    public $id;
    public $address;
    private $point;

    public function setPoint(Point $point)
    {
        $this->point = $point;
    }

    /**
     * @return Point
    */
    public function getPoint()
    {
        return $this->point;
    }

    static function load(array $data)
    {
        // TODO: Implement load() method.
    }

}