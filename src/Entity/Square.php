<?php
namespace App\Entity;

class Square
{
    private $point;
    public $radius;

    /**
     * @return mixed
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * @param mixed $point
     */
    public function setPoint(Point $point)
    {
        $this->point = $point;
    }



}