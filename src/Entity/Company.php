<?php
namespace App\Entity;

class Company implements IEntity
{
    public $id;
    public $name;
    private $building;
    public $phones;

    /**
     * @return Building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * @param mixed $building
     */
    public function setBuilding(Building $building)
    {
        $this->building = $building;
    }


    static function load(array $data)
    {
        // TODO: Implement load() method.
    }

}