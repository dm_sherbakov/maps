<?php
namespace App\Database\Tables;

class BaseTable
{
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}