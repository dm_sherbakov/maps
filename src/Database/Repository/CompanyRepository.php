<?php

namespace App\Database\Repository;

use App\Database\Tables\Company;
use App\Entity\Company as CompanyEntity;

class CompanyRepository extends BaseRepository
{
    public function __construct($em)
    {
        $class = $em->getClassMetadata(Company::class);
        parent::__construct($em, $class);
    }

    public function create(CompanyEntity $company)
    {
        $companyRow = new Company();
        $companyRow->setName($company->name);
        $companyRow->setPhones($company->phones);
        $building = $company->getBuilding();
        if ($building) {
            $companyRow->setBuildingId($building->id);
        }
        $this->getEntityManager()->persist($companyRow);
        $this->getEntityManager()->flush();
        return $companyRow->getArrayCopy();
    }

    public function update(CompanyEntity $company)
    {
        $companyRow = $this->find($company->id);
        if (!$companyRow) {
            throw new NotFoundRecordException("Company not found!");
        }
        $companyRow->setName($company->name);
        $companyRow->setPhones($company->phones);
        $building = $company->getBuilding();
        if ($building) {
            $companyRow->setBuildingId($building->id);
        }
        $this->getEntityManager()->flush();
        return $companyRow->getArrayCopy();
    }

    public function delete($id)
    {
        $company = $this->find($id);
        if (!$company) {
            throw new NotFoundRecordException('Company not found!');
        }
        $this->getEntityManager()->remove($company);
        $this->getEntityManager()->flush();
    }

}