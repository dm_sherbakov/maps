<?php

namespace App\Database\Repository;

use App\Database\Tables\Building;
use App\Entity\Building as BuildingEntity;
use App\Filters\BuildingFilter;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping;

class BuildingRepository extends BaseRepository
{
    public function __construct(EntityManager $em)
    {
        $class = $em->getClassMetadata(Building::class);
        parent::__construct($em, $class);
    }

    public function create(BuildingEntity $building)
    {
        $buildingRow = new Building();
        $buildingRow->setAddress($building->address);
        $buildingRow->setLatitude($building->getPoint()->latitude);
        $buildingRow->setLongitude($building->getPoint()->longitude);
        $this->getEntityManager()->persist($buildingRow);
        $this->getEntityManager()->flush();
        return $buildingRow->getArrayCopy();
    }

    public function update(BuildingEntity $building)
    {
        $buildingRow = $this->find($building->id);
        if(!$buildingRow){
            throw new NotFoundRecordException("Building not found!");
        }
        $buildingRow->setAddress($building->address);
        $buildingRow->setLatitude($building->getPoint()->latitude);
        $buildingRow->setLongitude($building->getPoint()->longitude);
        $this->getEntityManager()->flush();
        return $buildingRow->getArrayCopy();
    }

    public function delete($id){
        $building = $this->find($id);
        if(!$building) {
            throw new NotFoundRecordException("Building not found!");
        }
        $this->getEntityManager()->remove($building);
        $this->getEntityManager()->flush();
    }

    public function findByParams(BuildingFilter $params)
    {
        $criteria = Criteria::create();
        $queryBuilder = $this->createQueryBuilder(Building::class);
        if($params->buildingId){
            $criteria->where(Criteria::expr()->eq('id', $params->buildingId));
        }
        if($params->address){
            $criteria->where(Criteria::expr()->contains('address', $params->address));
        }
    }
}