<?php
namespace App\Database\Repository;

use App\Database\Tables\Category;
use App\Entity\Category as CategoryEntity;
use Doctrine\Common\Collections\Criteria;

class CategoryRepository extends BaseRepository
{
    public function __construct($em)
    {
        $class = $em->getClassMetadata(Category::class);
        parent::__construct($em, $class);
    }

    public function create(CategoryEntity $category)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $parentCategory = $this->find($category->parentCategory);
            if (!$parentCategory) {
                $maxRightRow = $this->findOneBy(array(), array('right_key' => Criteria::DESC));
                if (!$maxRightRow) {
                    $maxRight = 0;
                } else {
                    $maxRight = $maxRightRow->getRightKey() + 1;
                }
            } else {
                $maxRight = $parentCategory->getRightKey();
            }

            if ($parentCategory) {
                // UPDATE Category SET left_key = left_key + 2, right_key = right_ key + 2 WHERE left_key > :right_key
                $qb = $this->getEntityManager()->createQueryBuilder();
                $query = $qb->update(Category::class)
                    ->set('left_key', 'left_key + 2')
                    ->set('right_key', 'right_key + 2')
                    ->where('left_key > ?right_key')
                    ->setParameter('right_key', $maxRight)
                    ->getQuery();
                $query->execute();
            }

            // UPDATE my_tree SET right_key = right_key + 2 WHERE right_key >= $right_key AND left_key < $right_key
            $qb = $this->getEntityManager()->createQueryBuilder();
            $query = $qb->update(Category::class)
                ->set('right_key', 'right_key + 2')
                ->where('right_key >= ?right_key')
                ->andWhere('left_key < ?right_key')
                ->setParameter('right_key', $maxRight)
                ->getQuery();
            $query->execute();


            $categoryRow = new Category();
            $categoryRow->setName($category->name);
            $categoryRow->setLeftKey($maxRight);
            $categoryRow->setRightKey($maxRight + 1);

            $level = $parentCategory ? $parentCategory->getLevel() + 1 : 0;
            $categoryRow->setLevel($level);

            $this->getEntityManager()->persist($categoryRow);
            $this->getEntityManager()->flush();

            $this->getEntityManager()->getConnection()->commit();

            return $categoryRow->getArrayCopy();
        } catch(\Exception $ex){
            $this->getEntityManager()->getConnection()->rollBack();
            throw new InsertRecordException("Category insert error!", 0, $ex);
        }
    }

    public function delete($id)
    {
        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $category = $this->find($id);
            if (!$category) {
                throw new NotFoundRecordException("Category not found!");
            }

            // DELETE FROM category WHERE left_key >= $left_key AND right_ key <= $right_key
            $qb = $this->getEntityManager()->createQueryBuilder();
            $query = $qb->delete(Category::class)
                ->where('left_key >= ?left_key')
                ->andWhere('right_key <= ?right_key')
                ->setParameter('left_key', $category->getLeftKey())
                ->setParameter('right_key', $category->getRightKey())
                ->getQuery();
            $query->execute();

            // UPDATE category SET right_key = right_key – ($right_key - $left_key + 1) WHERE right_key > $right_key AND left_key < $left_key
            $qb = $this->getEntityManager()->createQueryBuilder();
            $query = $qb->update(Category::class)
                ->set('right_key', 'right_key - ?offset')
                ->where('right_key > ?right_key')
                ->andWhere('left_key < ?left_key')
                ->setParameter('?offset', $category->getRightKey() - $category->getLeftKey() + 1)
                ->setParameter('right_key', $category->getRightKey())
                ->setParameter('left_key', $category->getLeftKey())
                ->getQuery();
            $query->execute();

            // UPDATE category SET left_key = left_key – ($right_key - $left_key + 1), right_key = right_key – ($right_key - $left_key + 1) WHERE left_key > $right_key
            $qb = $this->getEntityManager()->createQueryBuilder();
            $query = $qb->update(Category::class)
                ->set('left_key', 'left_key - ?offset')
                ->set('right_key', 'right_key - ?offset')
                ->where('left_key > ?right_key')
                ->setParameter('offset', $category->getRigthKey() - $category->getLeftKey() + 1)
                ->setParameter('right_key', $category->getRightKey())
                ->getQuery();
            $query->execute();
            $this->getEntityManager()->getConnection()->commit();
        } catch(\Exception $ex){
            $this->getEntityManager()->getConnection()->rollBack();
            throw new DeleteRecordException("Category insert error!", 0, $ex);
        }
    }

    public function update(CategoryEntity $category)
    {
        $categoryRow = $this->find($category->id);
        if(!$category){
            throw new NotFoundRecordException('Category not found!');
        }

        // делаем только обновление имени категории, можно написать и перемещение по веткам, но это дольше чем удалить + создать,
        // правда создавать придётся и дочерние категории тоже
        $categoryRow->setName($category->name);
        $this->getEntityManager()->flush();
        return $categoryRow->getArrayCopy();
    }

    public function getChilds($id)
    {
        // SELECT id, name, level FROM category WHERE left_key >= $left_key AND right_key <= $right_key ORDER BY left_key
        $category = $this->find($id);
        if(!$category){
            throw new NotFoundRecordException("Category not found!");
        }
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->select('id, name, level')
            ->from(Category::class, 'c')
            ->where('left_key >= ?left_key')
            ->andWhere('right_key <= ?right_key')
            ->setParameter('left_key', $category->getLeftKey())
            ->setParameter('right_key', $category->getRightKey())
            ->orderBy('left_key')
            ->getQuery();
        return $query->execute();
    }
}